package org.com.kata.compte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 
 * @author Youssef CHAHID
 *
 */
@SpringBootApplication
public class KataCompteApplication {

	public static void main(String[] args) {
		SpringApplication.run(KataCompteApplication.class, args);
	}
}
